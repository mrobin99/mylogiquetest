-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.6-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for logique
CREATE DATABASE IF NOT EXISTS `logique` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `logique`;

-- Dumping structure for table logique.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(25) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT 1,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `dateofbirth` date DEFAULT NULL,
  `membershiptype` varchar(50) DEFAULT NULL,
  `cardnumber` int(50) DEFAULT NULL,
  `cvc` varchar(50) DEFAULT NULL,
  `cardtype` varchar(50) DEFAULT NULL,
  `exipredmonth` varchar(50) NOT NULL DEFAULT '0',
  `exipredyear` year(4) NOT NULL DEFAULT 2000,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table logique.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `password`, `status`, `created`, `modified`, `address`, `dateofbirth`, `membershiptype`, `cardnumber`, `cvc`, `cardtype`, `exipredmonth`, `exipredyear`) VALUES
	(1, 'Muhammad', 'Robin', 'cingobean@gmail.com', '$2y$10$MQY2XGLe5eZWAUTIPVYXp.r4XT.qmrDR7XhSIDyO8TfcnsnWZu/tC', 1, '2021-09-02 10:40:00', '2021-09-02 10:40:00', 'OK', '2021-09-21', 'Silver', NULL, NULL, NULL, '0', '2000');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
