<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Wizard-v1</title>
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Font-->
	<!-- <link rel="stylesheet" type="text/css" href="css/raleway-font.css"> -->
    <?= $this->Html->css('/formwizardcake/css/raleway-font.css'); ?>
	<!-- <link rel="stylesheet" type="text/css" href="fonts/material-design-iconic-font/css/material-design-iconic-font.min.css"> -->
    <?= $this->Html->css('/formwizardcake/fonts/material-design-iconic-font/css/material-design-iconic-font.min.css'); ?>
	<!-- Jquery -->
	<link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
	<!-- Main Style Css -->
    <!-- <link rel="stylesheet" href="css/style.css"/> -->
    <?= $this->Html->css('/formwizardcake/css/style.css'); ?>
</head>
<body>
	<div class="page-content" style="background-image: url('images/wizard-v1.jpg')">
		<div class="wizard-v1-content">
			<div class="wizard-form">
		        <form class="form-register" id="form-register" action="/users/add" method="post">
                <?= $this->Form->create($user) ?>
		        	<div id="form-total">
		        		<!-- SECTION 1 -->
			            <h2>
			            	<span class="step-icon"><i class="zmdi zmdi-account"></i></span>
			            	<span class="step-number">Step 1</span>
			            	<span class="step-text">Account Infomation</span>
			            </h2>
			            <section>
			                <div class="inner">
								<div class="form-row">
									<div class="form-holder form-holder-2">
										<label for="firstname">First Name*</label>
										<input type="text" placeholder="First Name" class="form-control" id="firstname" name="firstname" required>
									</div>
								</div>
                                <div class="form-row">
									<div class="form-holder form-holder-2">
										<label for="lastname">Last Name*</label>
										<input type="text" placeholder="Last Name" class="form-control" id="lastname" name="lastname" required>
									</div>
								</div>
								<div class="form-row">
									<div class="form-holder form-holder-2">
										<label for="email">Email Address*</label>
										<input type="email" placeholder="Your Email" class="form-control" id="email" name="email" required pattern="[^@]+@[^@]+.[a-zA-Z]{2,6}">
									</div>
								</div>
								<div class="form-row">
									<div class="form-holder">
										<label for="password">Password*</label>
										<input type="password" placeholder="Password" class="form-control" id="password" name="password" required >
									</div>
									<div class="form-holder">
										<label for="confirm_password">Confirm Password*</label>
										<input type="password" placeholder="Confirm Password" class="form-control" id="confirm_password" name="confirm_password" required>
									</div>
								</div>
                                <div>
                                    <div class="form-holder">
                                        <input id="acceptTerms" name="acceptTerms" type="checkbox" class="required valid">
                                        I agree with the Terms and Conditions
									</div>
                                </div>
							</div>
			            </section>
						<!-- SECTION 2 -->
			            <h2>
			            	<span class="step-icon"><i class="zmdi zmdi-card"></i></span>
			            	<span class="step-number">Step 2</span>
			            	<span class="step-text">Payment Infomation</span>
			            </h2>
			            <section>
			                <div class="inner">
								<div class="form-row">
									<div class="form-holder form-holder-2">
										<label for="dateofbirth">Date Of Birth*</label>
										<input type="date" placeholder="Date Of Birth" class="form-control" id="dateofbirth" name="dateofbirth" required>
									</div>
								</div>
                                <div class="form-row">
									<div class="form-holder form-holder-2">
										<label for="address">Address*</label>
                                        <textarea  class="form-control" name="address" id="address" rows="8" cols="50"></textarea>
									</div>
								</div>
								<div class="form-row">
									<div class="form-holder form-holder-2">
										<label for="membershiptype">Membership Type</label>
										<select name="membershiptype" id="membershiptype" class="form-control" required>
											<option value="" disabled selected>Select Membership Type</option>
											<option value="Silver">Silver</option>
											<option value="Gold">Gold</option>
											<option value="Platinum">Platinum</option>
											<option value="Black">Black</option>
                                            <option value="VIP">VIP</option>
											<option value="VVIP">VVIP</option>
										</select>
									</div>
								</div>
								<div class="form-row">
									<div class="form-holder form-holder-3">
										<label for="card-number">Card Number</label>
										<input type="text" name="card-number" class="cardnumber" id="card-number" placeholder="ex: 489050625008xxxx" maxlength="15" required>
									</div>
									<div class="form-holder">
										<label for="cvc">CVC</label>
										<input type="text" name="cvc" class="cvc" id="cvc" placeholder="xxx" maxlength="3" required> 
									</div>
									<!-- <div class="form-holder">
										<label for="cvc">CVC</label>
										<input type="text" name="cvc" class="cvc" id="cvc" placeholder="xxx" maxlength="3">
									</div> -->
								</div>
								<div class="form-row">
									<div class="form-holder form-holder-3">
									<label for="card-type">Membership Type</label>
										<select name="cardtype" id="card-type" class="form-control" required>
											<option value="" disabled selected>Select Card Type</option>
											<option value="Silver">Visa</option>
											<option value="Gold">Master Card</option>
										</select>
									</div>
									<div class="form-holder">
									<label for="month">Expiry Month</label>
										<select name="exipredmonth" id="month" class="form-control" required>
											<option value="" disabled selected>Expiry Month</option>
											<option value="January">January</option>
											<option value="February">February</option>
											<option value="March">March</option>
											<option value="February">April</option>
											<option value="April">May</option>
											<option value="May">June</option>
										</select>
									</div>
									<div class="form-holder">
									<label for="year">Expired Year</label>
										<input type="text" placeholder="Expired Year" class="form-control" id="year" name="exipredyear" maxlength="4"required>
									</div>
								</div>
							</div>
			            </section>
			            <!-- SECTION 3 -->
			            <h2>
			            	<span class="step-icon"><i class="zmdi zmdi-shield-check"></i></span>
			            	<span class="step-number">Step 3</span>
			            	<span class="step-text">Confirm Your Details</span>
			            </h2>
			            <section>
                       
			                <div class="inner">
			                	<h3>Comfirm Details</h3>
								<div class="form-row table-responsive">
									<table class="table">
										<tbody>
											<tr class="space-row">
												<th>First Name:</th>
												<td id="firstname-val"></td>
											</tr>
                                            <tr class="space-row">
												<th>Last Name:</th>
												<td id="lastname-val"></td>
											</tr>
											<tr class="space-row">
												<th>Email Address:</th>
												<td id="email-val"></td>
											</tr>
											<tr class="space-row">
												<th>Card Type:</th>
												<td id="card-type-val"></td>
											</tr>
											<tr class="space-row">
												<th>Card Number:</th>
												<td id="card-number-val"></td>
											</tr>
											<tr class="space-row">
												<th>CVC:</th>
												<td id="cvc-val"></td>
											</tr>
											<tr class="space-row">
												<th>Expiry Month:</th>
												<td id="month-val"></td>
											</tr>
											<tr class="space-row">
												<th>Expiry Year:</th>
												<td id="year-val"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<!-- <input type="submit" value="Submit"> -->
			            </section>
		        	</div>
                    <?= $this->Form->end() ?>
		        </form>
			</div>
		</div>
	</div>
	<!-- <script src="js/jquery-3.3.1.min.js"></script> -->
    <?= $this->Html->script('/formwizardcake/js/jquery-3.3.1.min.js'); ?>
	<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
	<!-- <script src="js/jquery.steps.js"></script>
	<script src="js/main.js"></script> -->
    <?= $this->Html->script('/formwizardcake/js/jquery.steps.js'); ?>
    <?= $this->Html->script('/formwizardcake/js/main.js'); ?>
</body>
</html>